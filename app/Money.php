<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Money extends Model
{
    protected $table='monies';

    protected $fillable = [ 'balance', 'withdraw', 'deposite', 'user_id', ];

    
}
