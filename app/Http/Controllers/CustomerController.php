<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Money;
use Validator;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    public function addFormCustomer()
    {
    	return view('add-customer');
    }

    public function addCustomer(Request $data)
    {
    	$this->validate($data,[
    		'email' => 'required|email|unique:customers',
    		'name' => 'required|min:2',
    		'lastname' => 'required|min:3',
    		'gender' => 'required',
       	]);

       	$addData = new Customer;
       	$addData->email = $data->email;
       	$addData->name = $data->name;
       	$addData->lastname = $data->lastname;
       	$addData->gender = $data->gender;
       	$addData->bonus = rand(5,20);
       	$addData->save();
       	$lastID = $addData->id;
       	$addId = new Money;
       	$addId->customer_id = $lastID;
       	$addId->save();
       	return redirect('/');
    }

    public function listCustomer()
    {
    	$customers = Customer::all();
    	//->toJson();
    	return view('listcustomers', ['customers'=>$customers]);
	
    }

    public function editCustomer($id)
    {
    	$data = Customer::find($id);
    	
    	return view('editcustomer', ['data'=>$data]);
    }

    public function updateCustomer(Request $data)
    {
    	$this->validate($data,[
    		'email' => 'required|email',
    		'name' => 'required|min:2',
    		'lastname' => 'required|min:3',
    		'bonus' => 'required|integer|between:5,20',
    		'gender' => 'required',
       	]);

    	$update = Customer::find($data->id);
    	$update->email = $data->email;
    	$update->name = $data->name;
    	$update->lastname = $data->lastname;
      $update->gender = $data->gender;
      $update->bonus = $data->bonus;
      $update->update();
      return redirect()->back();

    }
    public function index($id)
    {
      $data = Customer::find($id);
      $id = $data->id;
      $balance = DB::table('monies')->where('customer_id', $id)->latest()->first();
      $array = json_decode(json_encode($balance), true);
      $currentBalance = $array['balance'];
     
      return view('balance',[
        'data' => $data, 
        'currentBalance' => $currentBalance
        ]);
    }
    
}
