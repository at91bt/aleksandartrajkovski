<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Money;
use App\Customer;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\RedirectResponse;

class MoneyController extends Controller
{
    public function deposite(Request $request)
    {
    	$id = $request->id;
    	$count = count(DB::table('monies')->where('customer_id', $id)->where('deposite', '>', 0)->pluck('customer_id'));
       	$balance = DB::table('monies')->where('customer_id', $id)->latest()->first();
    	$decodeBalance = json_decode(json_encode($balance), true);
    	$currentBalance = $decodeBalance['balance'];
    	$bonus = DB::table('customers')->where('id', $id)->pluck('bonus');
    	$decodeBonus = json_decode(json_encode($bonus), true);
    	$currentBonus = $decodeBonus[0];
    	
    	
	   		$data = new Money;
	    	$data->customer_id = $request->id;
	    	$data->deposite = $request->deposite;
	    if ($count > 0 && ($count + 1) % 3 === 0) {
	   		$data->balance = $currentBalance + ($request->deposite *= ( 1 + ($currentBonus / 100))); }
	    else {
			$data->balance = $currentBalance + $request->deposite;
	    }
	    	$data->save();
	    return redirect()->back();
    }
    

    public function withdraw(Request $request)
    {
    	$id = $request->id;
       	$balance = DB::table('monies')->where('customer_id', $id)->latest()->first();
    	$array = json_decode(json_encode($balance), true);
    	$currentBalance = $array['balance'];

    		$data = new Money;
    		$data->customer_id = $request->id;
    		$data->withdraw = $request->withdraw;
    	if ($currentBalance >= 0 ){
    		return redirect()->back()->with('status', 'You do not have enough money!!!');}
    	else {
       		$data->balance = $currentBalance - $request->withdraw;
       	}
    	$data->save();
    	return redirect()->back();
    
    }

    public function history($id)
    {
    	$data = Customer::find($id)->moneyBalance;
    	
    	return $data->toJson();
    }

}
