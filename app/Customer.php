<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table='customers';

    protected $fillable = [ 'email', 'name', 'lastname', 'gender' ];

    public function moneyBalance()
    {
    	return $this->hasMany('App\Money', 'customer_id');
    }

}
