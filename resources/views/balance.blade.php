<!DOCTYPE html>
<html>
<head>
	<title>Balance</title>
</head>

<center><h3>{{ $data->name }}</h3></center>
Your account balance is: 
{{ $currentBalance }}
<br><hr><br>
<Strong><span>On every third deposite you will get your bonus!</span></Strong>
<br><br>
<form action="{{ route('deposite') }}" method="POST">
{{ csrf_field () }}
<input type="hidden" name="id" value="{{ $data->id }}">
	<label>Deposite</label>
	<input type="number" name="deposite" placeholder="Amount of cash to deposite">
	<br><br>
	<button type="submit">Deposite</button>
</form>
<br><hr><br>
<form action="{{ route('withdraw') }}" method="POST">
{{ csrf_field () }}
<input type="hidden" name="id" value="{{ $data->id }}">
	<label>Withdraw</label>
	<input type="number" name="withdraw" placeholder="Amount of cash to withdraw">
	<br><br>
	<button type="submit">Withdraw</button>
</form>
@if (session('status'))
     <div class="alert alert-success">
         {{ session('status') }}
     </div>
@endif   
<br><hr><br>
<a href="/"><button>Home</button></a>
<a href="/listcustomers"><button>List Costumers</button></a>

</body>
</html>




