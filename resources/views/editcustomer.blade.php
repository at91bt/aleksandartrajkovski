<!DOCTYPE html>
<html>
<head>
	<title>Edit Customer</title>
</head>
<body>
<center>
	<strong><p>Edit customer with E-MAIL: {{ $data->email }}</p></strong>
	<form action="{{ route('updateCustomer') }}" method="POST">
	{{ csrf_field () }}
		<label for="id">ID:</label>
		<input type="number" name="id" value="{{ $data->id }}" readonly>
		<br/><br/>
		<label for="email">Email Adress:</label>
		<input type="email" name="email" value="{{$data->email}}">
		<br/>
		<span>{{ $errors->first('email') }}</span>
		<br/><br/>
		<label for="name">Name:</label>
		<input type="text" name="name" value="{{$data->name}}">
		<br/>
		<span>{{ $errors->first('name') }}</span>
		<br/><br/>
		<label for="lastname">Lastname:</label>
		<input type="text" name="lastname" value="{{$data->lastname}}">
		<br/>
		<span>{{ $errors->first('lastname') }}</span>
		<br/><br/>
		<label for="bonus">Bonus % :</label>
		<input type="number" name="bonus" value="{{$data->bonus}}">
		<br/>
		<span>{{ $errors->first('bonus') }}</span>
		<br/><br/>
		<label>Gender:</label>
		<select name="gender">
			@if ($data->gender == "male"){
			<option value="male">Male</option>
			<option value="female">Female</option>
			}@else {
			<option value="female">Female</option>
  			<option value="male">Male</option>
  			}
  			@endif
  		</select>
  		<br/>
  		<span>{{ $errors->first('gender') }}</span>
  		<hr>
  		<button type="submit">Submit</button>
	</form>
	<br><br>
	<a href="/"><button>Home</button></a>
</center>
</body>
</html>