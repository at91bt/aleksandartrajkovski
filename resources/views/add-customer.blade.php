<!DOCTYPE html>
<html>
<head>
	<title>Add Customer</title>
</head>
<body>
<center>
	<h2>Add your Customer</h2>
	<br/>
	<form action="{{ route('addCustomer') }}" method="POST">
	{{ csrf_field () }}
		<p><strong>We will give random 5% to 20% bonus for every customer.</strong></p>
		<br/>
		<label for="email">Email Adress:</label>
		<input type="email" name="email">
		<br/>
		<span>{{ $errors->first('email') }}</span>
		<br/><br/>
		<label for="name">Name:</label>
		<input type="text" name="name">
		<br/>
		<span>{{ $errors->first('name') }}</span>
		<br/><br/>
		<label for="lastname">Lastname:</label>
		<input type="text" name="lastname">
		<br/>
		<span>{{ $errors->first('lastname') }}</span>
		<br/><br/>
		<label>Gender:</label>
		<select name="gender">
			<option></option>
  			<option value="male">Male</option>
  			<option value="female">Female</option>
  		</select>
  		<br/>
  		<span>{{ $errors->first('gender') }}</span>
  		<hr>
  		<button type="submit">Submit</button>
	</form>
	<br><br>
	<a href="/"><button>Home</button></a>
</center>
</body>
</html>