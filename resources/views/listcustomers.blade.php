<!DOCTYPE html>
<html>
<head>
	<title>List Customers</title>
</head>
<body>
<center>
<a href="/"><button>Home</button></a>
	<table>
		<thead>
			<tr>
			<th>ID:</th>
			<th>E-Mail:</th>
			<th>Name:</th>
			<th>Lastname:</th>
			<th>Gender:</th>
			<th>Bonus:</th>
			<th>Options:</th>
			<th>Money:</th>
			</tr>
		</thead>
		<tbody>
			@foreach($customers as $customer)
			<tr>
			<td>{{ $customer->id }}</td>
			<td>{{ $customer->email }}</td>
			<td>{{ $customer->name }}</td>
			<td>{{ $customer->lastname }}</td>
			<td>{{ $customer->gender }}</td>
			<td>{{ $customer->bonus }} %</td>
			<td><a href="/customer/edit/{{ $customer->id }}"><button>Edit</button></a></td>
			<td><a href="/balance/{{ $customer->id }}"><button>Balance</button></a></td>
			<td><a href="/history/{{ $customer->id }}"><button>History</button></a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
</center>
</body>
</html>