<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/add-customer')
->uses('CustomerController@addFormCustomer');

Route::post('/addedcustomer')
->name('addCustomer')
->uses('CustomerController@addCustomer');

Route::get('/listcustomers')
->uses('CustomerController@listCustomer');

Route::get('/customer/edit/{id}')
->uses('CustomerController@editCustomer');

Route::post('/updated')
->name('updateCustomer')
->uses('CustomerController@updateCustomer');

Route::get('/balance/{id}')
->uses('CustomerController@index');

Route::post('/deposite')
->name('deposite')
->uses('MoneyController@deposite');

Route::post('/withdraw')
->name('withdraw')
->uses('MoneyController@withdraw');

Route::get('/history/{id}')
->name('history')
->uses('MoneyController@history');

